﻿#include <cstdint>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <algorithm>
#include <iterator>
#include <vector>
#include "../include/utf8cpp/utf8.h"

using namespace std;
using namespace std::experimental::filesystem::v1;
using namespace utf8;

void Latin1ToUTF8(const path &input, const path &output)
{
    ifstream ifs(input, ios::binary);

    if (!ifs)
    {
        cout << "打开输入文件 " << input << " 失败。" << endl;
        return;
    }

    vector<uint32_t> wbuffer;
    string line;
    vector<char> u8buffer;

    u8buffer.assign(istreambuf_iterator<char>{ifs}, istreambuf_iterator<char>{});

    if (utf8::is_valid(u8buffer.begin(),u8buffer.end()))
    {
        return;
    }

    //Check if this file need to be converted.
    //Condition: non-ASCII characters not in comment
    ifs.seekg(0);
    while (getline(ifs, line))
    {
        if (line.empty() || line[0] == '#')
        {
            continue;
        }

        auto comment_pos = find(line.begin(), line.end(), '#');

        //Contains non-ASCII character, convert it
        if (find_if(line.begin(), comment_pos, [](char ch) {return static_cast<unsigned char>(ch) > 0x7F; }) != comment_pos)
        {
            transform(
                u8buffer.begin(),
                u8buffer.end(),
                back_inserter(wbuffer),
                [](char character)->uint32_t {return static_cast<unsigned char>(character); });

            u8buffer.clear();
            utf32to8(wbuffer.begin(), wbuffer.end(), back_inserter(u8buffer));

            create_directories(output.parent_path());

            ofstream ofs(output, ios::binary | ios::trunc);

            if (!ifs)
            {
                cout << "创建输出文件 " << output << " 失败。" << endl;
                return;
            }

            ofs.write(u8buffer.data(), u8buffer.size());
            return;
        }
    }
}

void Work(const path &iFolder, const path &oFolder)
{
    path iPath, oPath;

    if (!is_directory(iFolder))
    {
        cout << "参数不是有效的文件夹路径。" << endl;
        return;
    }

    recursive_directory_iterator iIt{ iFolder };

    while (iIt != recursive_directory_iterator{})
    {
        iPath = iIt->path();

        if (is_regular_file(iPath) && iPath.extension() == ".txt")
        {
            //拼接输出路径
            wstring f = iFolder;
            wstring i = iPath;
            wstring o = oFolder;

            wstring sub{ iPath, f.length(), wstring::npos };
            oPath = oFolder / sub;

            Latin1ToUTF8(iPath, oPath);
        }

        ++iIt;
    }
}

//递归
//.exe C:/input C:/output
int main(int argc, char *argv[])
{
    if (argc == 3)
    {
        Work(argv[1], argv[2]);
    }
    else if (argc == 1)
    {

    }

    return 0;
}
